package com.v2team.mobisysassignment.Utils;

/**
 * Created by v2team on 31/7/17.
 */

public class Constant {

    public static  final String APP_KEY = "b7cd3340a794e5a2f35e3abb820b497f";
    public final static String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";
    public static final String SUCCESS = "SUCCESS";
    public static final String SOME_SERVER_ERROR = "SOME SERVER ERROR";

    public enum responseStatus {
        DATANULL,
        SUCCESS,
    }
}
