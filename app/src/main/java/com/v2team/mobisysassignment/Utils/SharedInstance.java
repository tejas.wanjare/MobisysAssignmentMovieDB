package com.v2team.mobisysassignment.Utils;

import com.v2team.mobisysassignment.Models.ImageList;
import com.v2team.mobisysassignment.Models.MovieDetail;
import com.v2team.mobisysassignment.Models.MovieList;

import org.androidannotations.annotations.EBean;

/**
 * Created by v2team on 31/7/17.
 */

@EBean
public class SharedInstance {

    private static SharedInstance singleton = new SharedInstance();
    public static SharedInstance getInstance() {
        if (singleton == null) {
            singleton = new SharedInstance();
        }
        return singleton;
    }

    public MovieList getMovieList() {
        return movieList;
    }

    public void setMovieList(MovieList movieList) {
        this.movieList = movieList;
    }

    private MovieList movieList;

    public MovieList getMasterMovieList() {
        return masterMovieList;
    }

    public void setMasterMovieList(MovieList masterMovieList) {
        this.masterMovieList = masterMovieList;
    }

    private MovieList masterMovieList;

    public ImageList getImageList() {
        return imageList;
    }

    public void setImageList(ImageList imageList) {
        this.imageList = imageList;
    }

    private ImageList imageList;

    public MovieDetail getMovieDetail() {
        return movieDetail;
    }

    public void setMovieDetail(MovieDetail movieDetail) {
        this.movieDetail = movieDetail;
    }

    private MovieDetail movieDetail;
}
