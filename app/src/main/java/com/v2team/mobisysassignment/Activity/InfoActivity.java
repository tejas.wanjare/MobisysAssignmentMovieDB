package com.v2team.mobisysassignment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.v2team.mobisysassignment.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_info)
public class InfoActivity extends AppCompatActivity {

    @ViewById(R.id.developersDetailsTextView)
    TextView developersDetailsTextView;

    @AfterViews
    void afterViews(){
        developersDetailsTextView.setText(getResources().getString(R.string.developer_details));
    }
}
