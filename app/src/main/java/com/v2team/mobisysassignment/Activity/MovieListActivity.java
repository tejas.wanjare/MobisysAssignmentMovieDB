package com.v2team.mobisysassignment.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.v2team.mobisysassignment.Adapter.MovieListAdapter;
import com.v2team.mobisysassignment.BusinessEvent.MovieEvents;
import com.v2team.mobisysassignment.Models.MovieList;
import com.v2team.mobisysassignment.Models.Result;
import com.v2team.mobisysassignment.R;
import com.v2team.mobisysassignment.Utility.EndlessScrollListener;
import com.v2team.mobisysassignment.Utility.Utility;
import com.v2team.mobisysassignment.Utils.Constant;
import com.v2team.mobisysassignment.Utils.SharedInstance;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_movie_list_screen)
public class MovieListActivity extends AppCompatActivity {

    @ViewById(R.id.movieListView)
    ListView movieListView;

    ProgressDialog progressDialog;

    @Bean
    MovieEvents movieEvents;

    int pageNo = 1;

    MovieListAdapter movieListAdapter;
    private MovieList movieList;
    List<Result> resultList;
    List<Result> resultsSubList;
    Resources resource;


    @AfterViews
    void afterviews() {
        resource = getResources();
        resultList = new ArrayList<>();

        if(Utility.isNetworkAvailable(MovieListActivity.this)) {
            createProgressDialog();
            getMovieList();
        }
        else {
            Utility.showAlertDialog(MovieListActivity.this,"No Internet Connection","Please check your internet connection");
        }
        movieListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                pageNo = page;
                if(Utility.isNetworkAvailable(MovieListActivity.this)) {
                    createProgressDialog();
                    getMovieList();
                }
                else {
                    Utility.showAlertDialog(MovieListActivity.this,"No Internet Connection","Please check your internet connection");
                }
                return true;
            }
        });
        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MovieListActivity.this,MovieDetailActivity_.class);
                i.putExtra("movie_id",resultList.get(position).getId().toString());
                startActivity(i);
            }
        });

    }

    @UiThread
    void createProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(resource.getString(R.string.loading));
            progressDialog.show();
        }
    }

    @Background
    void getMovieList() {
        String result = Constant.responseStatus.DATANULL.toString();

        try {
            Constant.responseStatus movieListEnum = movieEvents.getMoviesListEvent(Integer.toString(pageNo));

            if (movieListEnum != null && movieListEnum.toString().equalsIgnoreCase(Constant.SUCCESS)) {
                result = Constant.SUCCESS;
            } else {
                result = Constant.SOME_SERVER_ERROR;
            }
        } finally {
            publishResultMovieList(result);
        }
    }

    @UiThread
    void publishResultMovieList(String result) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (result.equalsIgnoreCase(Constant.SUCCESS)) {
            movieList = SharedInstance.getInstance().getMovieList();
            displayMovieData();
        } else {
            Utility.showAlertDialog(MovieListActivity.this, resource.getString(R.string.ERROR), result);
        }
    }

    @UiThread
    void displayMovieData() {

        progressDialog.dismiss();
        if (movieList != null && ! movieList.getResults().isEmpty()) {
            resultsSubList = movieList.getResults();
            resultList.addAll(resultsSubList);
            if(pageNo == 1) {
                SharedInstance.getInstance().setMasterMovieList(movieList);
            }
            else {
                SharedInstance.getInstance().getMasterMovieList().getResults().addAll(resultsSubList);
            }
            movieListAdapter = new MovieListAdapter(MovieListActivity.this, resultList);
            movieListView.setAdapter(movieListAdapter);
            if (resultList != null && resultsSubList != null && !resultsSubList.isEmpty()) {
                movieListView.setSelection(resultList.size() - 24);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_info) {
            Intent i  = new Intent(MovieListActivity.this , InfoActivity_.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
