package com.v2team.mobisysassignment.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.v2team.mobisysassignment.Adapter.SamplePagerAdapter;
import com.v2team.mobisysassignment.BusinessEvent.MovieEvents;
import com.v2team.mobisysassignment.Models.ImageList;
import com.v2team.mobisysassignment.Models.MovieDetail;
import com.v2team.mobisysassignment.R;
import com.v2team.mobisysassignment.Utility.Utility;
import com.v2team.mobisysassignment.Utils.Constant;
import com.v2team.mobisysassignment.Utils.SharedInstance;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import me.relex.circleindicator.CircleIndicator;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

@EActivity(R.layout.activity_movie_detail_screen)
public class MovieDetailActivity extends AppCompatActivity {

   /* @Extra("movie_id")
    String movieId;*/

    String movieId;
    private ProgressDialog progressDialog;

    @Bean
    MovieEvents movieEvents;
    MovieDetail movieDetail;
    ImageList imageList;
    @ViewById(R.id.overViewTextView)
    TextView overViewTextView;

    @ViewById(R.id.titleTextView)
    TextView titleTextView;

@ViewById(R.id.indicator)
    CircleIndicator indicator;

    @ViewById(R.id.viewPager)
    ViewPager viewPager;

    @ViewById(R.id.materialRatigBar)
    MaterialRatingBar materialRatigBar;
    private SamplePagerAdapter mAdapter;

    @AfterViews
    void  afterViews(){
        Intent intent = getIntent();
        movieId = intent.getStringExtra("movie_id");
        createProgressDialog();
        if(Utility.isNetworkAvailable(MovieDetailActivity.this)) {
            getMovieDetail();
        }
        else {
            Utility.showAlertDialog(MovieDetailActivity.this,"No Internet Connection","Please check your internet connection");
        }
    }

    @UiThread
    void createProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.show();
        }
    }

    @Background
    void getMovieDetail() {
        String result = Constant.responseStatus.DATANULL.toString();

        try {
            Constant.responseStatus movieListEnum = movieEvents.getMovieDetail(movieId);

            if (movieListEnum != null && movieListEnum.toString().equalsIgnoreCase(Constant.SUCCESS)) {
                result = Constant.SUCCESS;
            } else {
                result = Constant.SOME_SERVER_ERROR;
            }
        } finally {
            publishResultMovieList(result);
        }
    }

    @UiThread
    void publishResultMovieList(String result) {
        if (result.equalsIgnoreCase(Constant.SUCCESS)) {
            movieDetail = SharedInstance.getInstance().getMovieDetail();  mAdapter = new SamplePagerAdapter(1) {
                @Override public int getItemPosition(Object object) {
                    return POSITION_NONE;
                }
            };


            getImages();
        } else {
            Utility.showAlertDialog(MovieDetailActivity.this, getResources().getString(R.string.ERROR), result);
        }

    }

    @Background
    void getImages() {
        String result = Constant.responseStatus.DATANULL.toString();

        try {
            Constant.responseStatus movieListEnum = movieEvents.getImages(movieId);

            if (movieListEnum != null && movieListEnum.toString().equalsIgnoreCase(Constant.SUCCESS)) {
                result = Constant.SUCCESS;
            } else {
                result = Constant.SOME_SERVER_ERROR;
            }
        } finally {
            publishResultImages(result);
        }
    }

    @UiThread
    void publishResultImages(String result) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        if (result.equalsIgnoreCase(Constant.SUCCESS)) {
            imageList = SharedInstance.getInstance().getImageList();
            displayData();
        } else {
            Utility.showAlertDialog(MovieDetailActivity.this, getResources().getString(R.string.ERROR), result);
        }
    }

    void displayData() {
         overViewTextView.setText(movieDetail.getOverview());

        titleTextView.setText(movieDetail.getTitle());
        viewPager.setAdapter(new SamplePagerAdapter());
        indicator.setViewPager(viewPager);
        viewPager.setCurrentItem(2);
        materialRatigBar.setNumStars(5);
        materialRatigBar.setProgress(SharedInstance.getInstance().getMovieDetail().getVoteCount());
        materialRatigBar.setDrawingCacheBackgroundColor(getResources().getColor(R.color.cast_expanded_controller_background_color));
    }
}
