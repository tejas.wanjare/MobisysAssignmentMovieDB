package com.v2team.mobisysassignment.Services;


import com.v2team.mobisysassignment.Models.ImageList;
import com.v2team.mobisysassignment.Models.MovieDetail;
import com.v2team.mobisysassignment.Models.MovieList;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

//@Rest(rootUrl = "http://192.168.15.15:8081/", converters = {StringHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class, FormHttpMessageConverter.class}, interceptors = {HeadersRequestInterceptor.class})
@Rest(converters = {StringHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class})
public interface RestClient
{

	void setRootUrl(String rootUrl);

	//get popular call
	@Get("movie/upcoming?api_key={api_key}&language=en-US&page={page_no}")					//https://api.themoviedb.org/3/movie/upcoming
	MovieList getPopularList(@Path String api_key, @Path String page_no);

	//get Detail call
	@Get("movie/{movie_id}?api_key={api_key}")                                               //https://api.themoviedb.org/3/movie/<movie-id>
	MovieDetail getMovieDetail(@Path String api_key, @Path int movie_id);


	@Get("movie/{movie_id}/images?api_key={api_key}&language=en-US")                     	//https://api.themoviedb.org/3/movie/{movie_id}/images?api_key=<<api_key>>&language=en-US
	ImageList getImages(@Path String api_key, @Path String movie_id);
}
