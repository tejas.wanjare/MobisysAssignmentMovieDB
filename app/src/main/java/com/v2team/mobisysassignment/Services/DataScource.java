package com.v2team.mobisysassignment.Services;

import com.v2team.mobisysassignment.Models.ImageList;
import com.v2team.mobisysassignment.Models.MovieDetail;
import com.v2team.mobisysassignment.Models.MovieList;
import com.v2team.mobisysassignment.Utils.Constant;

import org.androidannotations.annotations.EBean;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.client.RestClientException;

import java.io.IOException;

/**
 * Created by v2team on 3/7/17.
 */
@EBean
public class DataScource {

    @RestService
    RestClient restClient;

    public MovieList getPopularList(String pageNo) throws HttpMessageConversionException, IOException {
        MovieList data = null;
        try {
            //set server here
            restClient.setRootUrl("https://api.themoviedb.org/3/");
            data = restClient.getPopularList(Constant.APP_KEY, pageNo);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return data;
    }

    public MovieDetail getMovieDetail(String movieID) throws HttpMessageConversionException, IOException {
        MovieDetail data = null;

        try {

            restClient.setRootUrl("https://api.themoviedb.org/3/");
            data = restClient.getMovieDetail(Constant.APP_KEY, Integer.parseInt(movieID));

            if (data != null) {
            }
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ImageList getImageList(String movieID) throws HttpMessageConversionException, IOException {
        ImageList data = null;

        try {

            restClient.setRootUrl("https://api.themoviedb.org/3/");
            data = restClient.getImages(Constant.APP_KEY, movieID);

            if (data != null) {
            }
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return data;
    }

}
