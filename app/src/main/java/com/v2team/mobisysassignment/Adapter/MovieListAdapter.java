package com.v2team.mobisysassignment.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.v2team.mobisysassignment.Models.Result;
import com.v2team.mobisysassignment.R;

import java.util.List;

/**
 * Created by v2team on 31/7/17.
 */

public class MovieListAdapter extends BaseAdapter {
    List<Result> resultList;
    LayoutInflater inflater;
    ImageView imageView;
    TextView movieOverViewTextView ,releaseDateTextView, ageFactor;
    Context context;

    public MovieListAdapter(Context context , List<Result> resultList){
        this.context = context;
        this.resultList=resultList;
    }
    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Object getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return resultList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.grid_view_item, parent, false);

        imageView = (ImageView) convertView.findViewById(R.id.imageView);
        movieOverViewTextView = (TextView) convertView.findViewById(R.id.movieOverViewTextView);
        releaseDateTextView = (TextView) convertView.findViewById(R.id.releaseDateTextView);
        ageFactor = (TextView) convertView.findViewById(R.id.ageFactor);

        if(resultList.get(position).getAdult()){
            ageFactor.setText("(A)");
        }
        else {
            ageFactor.setText("(U/A)");
        }

        releaseDateTextView.setText(resultList.get(position).getReleaseDate().toString());
        Log.w("getView", "resultList.get(position).getReleaseDate().toString()" +resultList.get(position).getReleaseDate().toString());
        movieOverViewTextView.setText(resultList.get(position).getOverview().toString());
        Picasso.with(context).load(resultList.get(position).getPosterPath()).into(imageView);
        return convertView;
    }
}
