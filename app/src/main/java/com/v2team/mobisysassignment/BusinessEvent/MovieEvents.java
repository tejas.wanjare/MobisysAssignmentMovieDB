package com.v2team.mobisysassignment.BusinessEvent;

import com.v2team.mobisysassignment.BusinessValidations.MovieListValidation;
import com.v2team.mobisysassignment.Services.DataScource;
import com.v2team.mobisysassignment.Utils.Constant;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.io.IOException;

/**
 * Created by v2team on 31/7/17.
 */

@EBean
public class MovieEvents {

    @Bean
    DataScource dataSource;

    public Constant.responseStatus getMoviesListEvent(String pageNo) {
        Constant.responseStatus movieListEnum = Constant.responseStatus.DATANULL;
        try {
            movieListEnum = MovieListValidation.ValidateGetPopularVideos((dataSource.getPopularList(pageNo)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movieListEnum;
    }

    public Constant.responseStatus getImages(String movieID) {
        Constant.responseStatus movieListEnum = Constant.responseStatus.DATANULL;
        try {
            movieListEnum = MovieListValidation.ValidateGetImages((dataSource.getImageList(movieID)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movieListEnum;
    }

    public Constant.responseStatus getMovieDetail(String movieID) {
        Constant.responseStatus movieListEnum = Constant.responseStatus.DATANULL;
        try {
            movieListEnum = MovieListValidation.ValidateGetMovieDetail((dataSource.getMovieDetail(movieID)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movieListEnum;
    }

}
