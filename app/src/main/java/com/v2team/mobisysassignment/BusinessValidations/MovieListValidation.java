package com.v2team.mobisysassignment.BusinessValidations;

import com.v2team.mobisysassignment.Models.ImageList;
import com.v2team.mobisysassignment.Models.MovieDetail;
import com.v2team.mobisysassignment.Models.MovieList;
import com.v2team.mobisysassignment.Utils.Constant;
import com.v2team.mobisysassignment.Utils.SharedInstance;

import org.androidannotations.annotations.EBean;

/**
 * Created by v2team on 31/7/17.
 */

@EBean
public class MovieListValidation {
    public static Constant.responseStatus ValidateGetPopularVideos(MovieList movieList) {

        if (movieList != null && !movieList.getResults().isEmpty()) {
            SharedInstance.getInstance().setMovieList(movieList);
            return Constant.responseStatus.SUCCESS;
        } else {
           return Constant.responseStatus.DATANULL;
        }
    }

    public static Constant.responseStatus ValidateGetImages(ImageList imageList) {

        if (imageList != null ) {
            SharedInstance.getInstance().setImageList(imageList);
            return Constant.responseStatus.SUCCESS;
        } else {
            return Constant.responseStatus.DATANULL;
        }
    }

    public static Constant.responseStatus ValidateGetMovieDetail(MovieDetail movieDetail) {

        if (movieDetail != null) {
            SharedInstance.getInstance().setMovieDetail(movieDetail);
            return Constant.responseStatus.SUCCESS;
        } else {
            return Constant.responseStatus.DATANULL;
        }
    }
}
